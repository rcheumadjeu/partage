pipeline{
    agent any
    stage('clone') {
        steps {
            sh "rm -rf *"
            sh "git clone https://github.com/priximo/jenkins-helloworld"
        }
    }
    stage('build') {
        steps {
            sh "cd jenkins-helloworld/ && javac Main.java"
        }
    }
    stage('run') {
        steps {
            sh "cd jenkins-helloworld/ && java Main"
        }
    }
}