node {
    stage('clone'){
        git 'https://github.com/priximo/ansible-jenkins.git'
    }
    stage('ansible') {
        ansiblePlaybook (
            colorized: true,
            become: true,
            playbook: 'plabook.yml',
            inventory: 'hosts.yml',
            extras: '--extra-vars "mavariable=${MAVARIABLE}"'
        )
    }
}

############################
node {

    def registryprojet='registry.gitlab.com****'
    de IMAGE= "${registryprojet}:version-${env.BUILD_ID}"
    stage('build - clone'){
        git 'https://github.com/priximo/war-build-docker.git'
    }
    stage('build -maven package'){
        sh 'mvn package'
    }
def img = stage('build') {
    docker.build ("$IMAGE", '.')
}
    stage('build - test'){
        img.withRun("--name run-$BUILD_ID -p 8081:8080")
        {c ->
        sh 'docker ps'
        sh 'netstat -ntaup'
        sh 'sleep 15s'
        sh 'curl 127.0.0.1:8081'
        sh 'docker ps'
        }
     }
     stage('build -push') {
        docker.withRegistry('https://registry.gitlab.com','reg1')
        img.push 'latest'
        img.push()
     }
     stage('deploy - clone'){
        git 'https://github.com/priximmo/jenkins-ansible-docker.git'
     }
     stage('deploy - end'){
        ansiblePlaybook (
         colorized: true,
         become: true,
         playbook: 'plabook.yml',
         inventory: 'hosts.yml',
         extras: "--extra-vars 'image=${IMAGE}'"
        )
     }
}

##############################################

script groovy for tags

import groovy.json.JsonSlurper;
def jsonSlurper=new JsonSlurper()
def nfile= "curl https://gitlab.com/api/v4/projects/42992603/repository/tags".execute().text
result= jsonSlurper.parseText(nfile) 
return result.name


node {
    
    
    stage('build'){
        
    }
    stage('run') {
        
    }
    stage('test') {
        
    }
}